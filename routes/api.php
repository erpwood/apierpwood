<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'cors'], function () {
   Route::post('user/login', 'APILoginController@login');
   Route::post('get-master-list', 'ErpWoodMasterController@index');
   Route::post('check-name', 'ErpWoodMasterController@verify');
   Route::post('import-master-data', 'ErpWoodMasterController@import');
   Route::post('delete-master', 'ErpWoodMasterController@delete');
   Route::post('get-master-details', 'ErpWoodMasterDetailsController@index');
   Route::post('store-master-details', 'ErpWoodMasterDetailsController@store');
   Route::post('fetchDetail','ErpWoodMasterDetailsController@getDetails');
   Route::post('tallyRecord/','ErpWoodMasterDetailsController@fetchTallyRecord');
   Route::post('nontallyRecord/','ErpWoodMasterDetailsController@fetchNonTallyRecord');
   Route::post('get-machine-list/','ErpWoodMachineController@index');
   Route::post('add-machine/','ErpWoodMachineController@insertRecord');
   Route::post('get-users/','UsersController@index');
   Route::post('create-user/','UsersController@createUser');
   Route::post('delete-machine-detail/','ErpWoodMachineController@destroy');
   Route::post('merge-non-telly','ErpWoodMasterDetailsController@mergeNontelly');
 });

// Route::group(['middleware' => 'cors'], function () {
//    Route::post('masterDetail/','ErpWoodMasterDetailsController@updateProcess');
//    Route::post('login/','UsersController@Login');
//    Route::post('tallyRecord/','ErpWoodMasterDetailsController@fetchTallyRecord');
//    Route::post('nontallyRecord/','ErpWoodMasterDetailsController@fetchNonTallyRecord');
//  });
