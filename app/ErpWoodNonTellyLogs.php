<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ErpWoodNonTellyLogs extends Eloquent
{
    //
    protected $connection = 'mongodb';
    protected $collection = 'erpwood_non_telly_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'barcode', 'lot','grade', 'length', 'sed', 'volume'
        //'name',
    ];
}
