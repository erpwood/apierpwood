<?php

namespace App;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\HybridRelations;

class ErpWoodMaster extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'erpwood_master';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'barcode', 'lot','grade', 'length', 'sed', 'volume'
        //'name',
    ];
}