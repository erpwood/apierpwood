<?php

namespace App\Http\Controllers;
use App\ErpWoodMasterDetails;
use App\ErpWoodMaster;
use App\ErpWoodNonTellyLogs;
use Illuminate\Http\Request;

class ErpWoodMasterDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $detailsObj = ErpWoodMasterDetails::all();
        if(isset($detailsObj)) { 
            foreach ( $detailsObj as $fm ) {

                $master_name = "Admin";//$fm->user->name;

                $fm["admin_name"] = $master_name;
               /* $fm["category_name"] = isset($fm->categories) ? $fm->categories->name:'';
                $fm["client_name"] = isset($fm->client) ? $fm->client->name :'';
                $fm["syllabus_name"] = isset($fm->syllabus) ? $fm->syllabus->name:'';
                $fm["qualification_name"] = isset($fm->qualification) ? $fm->qualification->name:'';*/
            }
            return response()->json([
                'status'   => 'success',
                'details_list' => $detailsObj],200);
        }
        else{
             return response()->json([
            'status'   => 'error',
            'msg' => "No Record found"],200);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
        public function store(Request $request)
    {
        $data = json_decode($_POST['data'],true);
        //return $data['data'];
        //return $data['process']['Barcode'];
        $dataObj = $data['data']; 
         if(isset($dataObj) && sizeof($dataObj) > 0){
             foreach ($dataObj as $dataObjMaster) {
                $dataObjProcess = $dataObjMaster['process'];
                foreach ($dataObjProcess as $dataObjProcessItem) {
                     $dataObjProcessItem['Cft'] = $dataObjProcessItem['AvgSED'];
                        $dataObjProcessItem['AvgSED'] = $this->ChangeInmeter($dataObjProcessItem['Cft']/3.14);
                        $dataObjProcessItem['CutLength'] = $this->ChangeInmeter($dataObjProcessItem['CutLength']);
                        $dataObjProcessItem['Volume'] =$this->CalVolume($dataObjProcessItem['AvgSED'],$dataObjProcessItem['CutLength']);
                    if(!isset($dataObjProcessItem['Barcode']) || empty($dataObjProcessItem['Barcode'])){
                        $this->createNonTellyRecord($dataObjProcessItem,$dataObjMaster['admin_id'],$dataObjMaster['client_id'],$dataObjMaster['master_id']);
                    }
                    else{
                        //return $dataObjMaster;
                         $model_details = ErpWoodMasterDetails::where('master_id',$dataObjMaster['master_id'])->where(['base.Barcode'=> $dataObjProcessItem['Barcode']])->first();
                         if(isset($model_details) && !empty($model_details) ){
                                $model_details->process      = $dataObjProcessItem;
                                $model_details->updated_by   = $dataObjMaster['admin_id'];
                                $model_details->client_id   = $dataObjMaster['client_id'];
                                $model_details->updated_at   = date("Y-m-d H:i:s");     
                                if(!$model_details->save()){
                                        error_log(print_r("Insert from vue".$model_details, TRUE));
                                }
                            }
                            else{
                                $this->createNonTellyRecord($dataObjProcessItem,$dataObjMaster['admin_id'],$dataObjMaster['client_id'],$dataObjMaster['master_id']);
                            }
                        }
                    } 
                }
            }
       

        //$model_master["admin_name"] = "Admin";
        return response()->json([
         'status'   => 'success',
         'msg' => "saved"
         ],200);
    }
    public function CalVolume($dia,$length){
        return round((3.14*(pow($dia,2))*$length)/4,2);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function createNonTellyRecord($model_obj,$adminId,$clientId,$masterId)
    {
        // return $model_obj;
        // $model_obj->Cft = $model_obj->AvgSED;
        // $model_obj->AvgSED = CalDiameterInmeter($model_obj);
        // return $model_obj;
        $model_nonTelly = new ErpWoodNonTellyLogs;
        $model_nonTelly->master_id    = $masterId;
        $model_nonTelly->client_id    = $clientId;
        $model_nonTelly->base         = [];
        $model_nonTelly->process      = $model_obj;
        $model_nonTelly->created_by   = $adminId;
        $model_nonTelly->updated_by   = $adminId;
        $model_nonTelly->created_at   = date("Y-m-d H:i:s");
        $model_nonTelly->updated_at   = date("Y-m-d H:i:s");
        $model_nonTelly->is_deleted   = 0;       
        if(!$model_nonTelly->save()){
                error_log(print_r("Insert from vue".$model_nonTelly, TRUE));
        }
        return true;
    }
    public function ChangeInmeter($number){
        return round($number*0.3048,2);
    }
    public function ChangeIncubicMeter($vol){
        return round($vol*0.0283168,2);

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function getDetails(Request $request)
    {
       $master_detail_list = ErpWoodMasterDetails::where('master_id',$request->input('masterId'))->get();
        return response()->json([
                 'status'   => 'success',
                 'master_detail' => $master_detail_list],200);
    }

    public function fetchTallyRecord(Request $request)
    {
       $master_detail_list = ErpWoodMasterDetails::where('master_id',$request->input('masterId'))->get();
       $tallyRecord = array();
       foreach ($master_detail_list as $singleRecord) 
       {
           if(isset($singleRecord->base) && isset($singleRecord->process))
                {
                 array_push($tallyRecord,$singleRecord);
                }
       }
       return response()->json([
                 'status'   => 'success',
                 'master_detail' => $tallyRecord],200);
    }
     
     public function fetchNonTallyRecord(Request $request)
    {
       $master_NonTelly_list = ErpWoodNonTellyLogs::where('master_id',$request->input('masterId'))->get();
       $NontallyRecord = array();
       foreach ($master_NonTelly_list as $singleRecord) 
       {  
           if(isset($singleRecord->process) && empty($singleRecord->base))
                {
                 array_push($NontallyRecord,$singleRecord);
                }
       }
       return response()->json([
                 'status'   => 'success',
                 'master_detail' => $NontallyRecord],200);
    }

     public function calculateDeviation($number1, $number2)
     {
        $deviation_array = array();
        array_push($deviation_array,$number2 - ($number1/100)*$number2);
        array_push($deviation_array,$number2 + ($number1/100)*$number2);
        return $deviation_array;
    }

     public function mergeNontelly(Request $request)
    {
        $nontelly = ErpWoodNonTellyLogs::where('master_id',$request->input('masterId'))->get();
        $unprocessMasterDetails =ErpWoodMasterDetails::where('master_id',$request->input('masterId'))->whereNull('process')->get();

        $exactmatchedMaster = array();
        $exactmatchedNonTelly =array();
        $percentmatchedMaster = array();
        $percentmatchedNonTelly =array();
        $unmatchedMasterDetails =array();
        // $unmatchedNonTellyDetails =array();

        foreach($unprocessMasterDetails as $singleRecord) {
            foreach($nontelly as $singleNontelly){
                    if( !$singleNontelly->is_matched == 1 ){
                        if($singleRecord->base['Volume'] == $singleNontelly->process['Volume'] && $singleRecord->base['AvgSED'] == $singleNontelly->process['AvgSED'] ){
                            $singleNontelly->is_matched = 1;
                            $singleNontelly->matched_with = $singleRecord->_id;
                            $singleNontelly->save();
                            array_push($exactmatchedMaster,$singleRecord);
                            array_push($exactmatchedNonTelly,$singleNontelly);
                            break;
                        }
                        else {
                            $dataDeviation = env('DATA_DEVIATION');
                            if($singleRecord->base['Volume'] == $singleNontelly->process['Volume'] && $singleRecord->base['AvgSED'] != $singleNontelly->process['AvgSED'] ){
                                $avgSED = $singleRecord->base['AvgSED'];
                                $i=1;
                                for($i;$i<=$dataDeviation;$i++){
                                    $Deviation =$this->calculateDeviation($i, $avgSED);
                                    if($Deviation[0] == $singleNontelly->process['AvgSED'] || $Deviation[1] == $singleNontelly->process['AvgSED'] )
                                    {
                                        $singleNontelly->is_matched = 1;
                                        $singleNontelly->matched_with = $singleRecord->_id;
                                        $singleNontelly->save();
                                        array_push($percentmatchedMaster,$singleRecord);
                                        array_push($percentmatchedNonTelly,$singleNontelly);
                                        break;
                                    } 
                                } 
                            } else{
                                if($singleRecord->base['Volume'] != $singleNontelly->process['Volume'] && $singleRecord->base['AvgSED'] == $singleNontelly->process['AvgSED']){
                                    $volume = $singleRecord->base['Volume'];
                                $i=1;
                                for($i;$i<=$dataDeviation;$i++){
                                    $Deviation =$this->calculateDeviation($i, $avgSED);
                                    if($Deviation[0] == $singleNontelly->process['Volume'] || $Deviation[1] == $singleNontelly->process['Volume'] )
                                    {
                                        $singleNontelly->is_matched = 1;
                                        $singleNontelly->matched_with = $singleRecord->_id;
                                        $singleNontelly->save();
                                        array_push($percentmatchedMaster,$singleRecord);
                                        array_push($percentmatchedNonTelly,$singleNontelly);
                                        break;
                                    } 
                                }
                            } 
                            else {
                                array_push($unmatchedMasterDetails,$singleRecord);
                            }
                        }
                    }    
                }           
            }
        }
        $unmatchedNonTellyDetails =  ErpWoodNonTellyLogs::where('master_id',$request->input('masterId'))->whereNull('is_matched')->get();
        
         return response()->json([
           'status' => 'success',
           'exact_matched_master_list' => $exactmatchedMaster,
           'exact_matched_nontelly_list' => $exactmatchedNonTelly,
           'deviation_matched_master_list' => $percentmatchedMaster,
           'deviation_matched_nontelly_list' => $percentmatchedNonTelly,
           'unmatched_master_list' => $unmatchedMasterDetails,
           'unmatched_nontelly_list' => $unmatchedNonTellyDetails
       ]);
    }
}
