<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ErpWoodMachine;

class ErpWoodMachineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $machineList = ErpWoodMachine::where('client_id',$request->input('client_id'))->get();
        return response()->json([
                'status' => 'success',
                'machine_list' => $machineList],200
            );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
       ErpWoodMachine::where('_id',$request->id)->delete();
       return response()->json([
            'status' => 'success',
            'msg' => 'Record is deleted.'
       ],200);
    }

    public function insertRecord()
    {  
        $data = json_decode($_POST['data'],true);

        $machine_model = new ErpWoodMachine;

        $machine_model->name = $data['name'];
        $machine_model->client_id = $data['client_id'];
        $machine_model->created_by = $data['admin_id'];
        $machine_model->created_at = date("Y-m-d H:i:s");
        $machine_model->updated_at = date("Y-m-d H:i:s");
        $machine_model->save();

        return response()->json([
            'status' => 'success',
            'machine_list' => $machine_model
        ],200);
    }
}
