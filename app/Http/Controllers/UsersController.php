<?php

namespace App\Http\Controllers;

use Validator;
use App\Users;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $users = Users::where( 'client_id',$request->input('client_id'))->get();
        return response()->json([
            'status' => 'success',
            'users_list' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    // public function Login(Request $request)
    // {
    //     $users = Users::where( 'username' , $request->input('email') )->where( 'password', $request->input('password'))->get();
    //     if(count($users)==1)
    //     {
    //         if ($users[0]->role == 'admin') {
    //            return response()->json(['status' => 'success','data' => $users],200);
    //         } else{
    //             return response()->json(['status' => 'accessible for only admin'],200);
    //         }
    //     }else
    //     {
    //         return response()->json(['status' => 'invalid credentials'],200);
    //     }
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createUser()
    { 
        $user_model = new Users;

        $user_model->username = $_POST['username'];
        $user_model->client_id = $_POST['client_id'];
        $user_model->password = $_POST['password'];
        $user_model->role = $_POST['role'];
        $user_model->contact = $_POST['contact'];

        $user_model->save();

        return response()->json([
            'status' => 'success',
            'new_user' => $user_model
        ],200);
    }
}
