<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Users;
use Illuminate\Support\Facades\Auth;

class APILoginController extends Controller
{
    public function login(Request $request)
    {
        $users = Users::where( 'username' , $request->input('email') )->where( 'password', $request->input('password'))->get();
        if(count($users)==1)
        {
            return response()->json(['status' => 'success','data' => $users],200);
        }else
        {
            return response()->json(['status' => 'invalid credentials'],200);
        }
    }

    // public function login(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'email' => 'required|string|email|max:255',
    //         'password'=> 'required',
    //         'role'=>'required'
    //     ]);
    //     if ($validator->fails()) {
    //         //return response()->json($validator->errors());
    //         return response()->json(['status' => 'invalid credentials'],200);
    //     }
    //     //$credentials = $request->only('email', 'password');
    //     /*$password = bcrypt($request->input('password'));
    //     $email = $request->input('email');*/
    //     //$credentials = ['email'=>$email,'password'=>$password];
    //     //return $_POST['role'];
    //     if(!isset($_POST['role'])) {
    //         return response()->json(['status' => 'Access denied! do not have login permission.'],200);
    //     }
    //     switch($_POST['role']){
    //         case 'admin':
    //         if($_POST['password'] == 'password' && $_POST['email'] == 'admin@admin.com'){
    //                 //$admin = []; // Protocol_user::where('id', '=' ,2)->first();
    //                 $admin = [
    //                           'id' => 2,
    //                           'client_id' => 8,
    //                           'name' => 'Admin',
    //                           'email' => 'admin@admin.com',
    //                           'role' => 1,
    //                           'created_at' => '2018-07-24 19:18:44',
    //                           'updated_at' => '2018-07-24 19:18:44',
    //                         ];
    //                 return response()->json(['status' => 'success','data'=>$admin],200);
    //             }
    //             else{
    //                 return response()->json(['status' => 'invalid credentials'],200);
    //             }
    //         break;
    //         case 'instructor':
            
    //         if($_POST['password'] == 'password' && $_POST['email'] == 'instructor1@instructor.com'){
    //                 //$instructor = Protocol_user::where('id', '=' ,6)->first();
    //                 return response()->json(['status' => 'success','id'=>6],200);
    //             }
                
    //             else if($_POST['password'] == 'password' && $_POST['email'] == 'instructor2@instructor.com'){
    //                 //$instructor = Protocol_user::where('id', '=' ,8)->first();
    //                 return response()->json(['status' => 'success','id'=>8],200);
    //             }
    //             else{
    //                 return response()->json(['status' => 'invalid credentials'],200);
    //             }
    //         break;
    //         default:
    //          return response()->json(['status' => 'Access denied! do not have login permission.'],200);
    //     }
		
    //     /*if($request->input('password') == 'password' && $request->input('email') == 'user@user.com'){
    //     	return response()->json(['status' => 'success'],200);
    //     }
    //     else{
    //     	return response()->json(['status' => 'invalid credentials'],200);
    //     }

    //     try {
    //         if (! $token = JWTAuth::attempt($credentials)) {
    //             return response()->json(['error' => 'invalid_credentials'], 401);
    //         }
    //     } catch (JWTException $e) {
    //         return response()->json(['error' => 'could_not_create_token'], 500);
    //     }
    //     return response()->json(compact('token')); */
    // }

    public function refresh()
    {
        return response([
         'status' => 'success'
        ]);
    }

    public function logout(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);
 
        try {
            JWTAuth::invalidate($request->token);
 
            return response()->json([
                'success' => true,
                'message' => 'User logged out successfully'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, the user cannot be logged out'
            ], 500);
        }
    }


}
