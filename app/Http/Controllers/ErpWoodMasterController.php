<?php

namespace App\Http\Controllers;

use App\ErpWoodMaster;
use Illuminate\Http\Request;
use App\ErpWoodMasterDetails;
use Validator;
class ErpWoodMasterController extends Controller
{
    /**
     * Display a listing of the resource.
          *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ErpWoodMaster = ErpWoodMaster::where('client_id',$request->input('client_id'))->get();
        // $ErpWoodMaster = ErpWoodMaster::all();
        if(isset($ErpWoodMaster)) { 
            foreach ( $ErpWoodMaster as $fm ) {

                $master_name = "Admin";//$fm->user->name;

                $fm["admin_name"] = $master_name;
               /* $fm["category_name"] = isset($fm->categories) ? $fm->categories->name:'';
                $fm["client_name"] = isset($fm->client) ? $fm->client->name :'';
                $fm["syllabus_name"] = isset($fm->syllabus) ? $fm->syllabus->name:'';
                $fm["qualification_name"] = isset($fm->qualification) ? $fm->qualification->name:'';*/
            }
            return response()->json([
                'status'   => 'success',
                'master_list' => $ErpWoodMaster],200);
        }
        else{
             return response()->json([
            'status'   => 'error',
            'msg' => "No Record found"],200);
        }
    }

     public function verify()
    {
       $ErpMaster = ErpWoodMaster::where('name',$_POST['name'])->where('client_id',$_POST['client_id'])->get();
        if(isset($ErpMaster) && count($ErpMaster) == 0) {
            return response()->json([
                'status' => 'success'
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'msg' => 'The consignment no. is already taken.'
            ]);
        }
    }

    public function delete()
    {
        ErpWoodMasterDetails::where('master_id',$_POST['id'])->delete();
        ErpWoodMaster::where('_id',$_POST['id'])->delete();

        return response()->json([
            'status' => 'success',
            'msg' => 'The consignment is deleted.'
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ErpWoodMaster  $erpWoodMaster
     * @return \Illuminate\Http\Response
     */
    public function show(ErpWoodMaster $erpWoodMaster)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ErpWoodMaster  $erpWoodMaster
     * @return \Illuminate\Http\Response
     */
    public function edit(ErpWoodMaster $erpWoodMaster)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ErpWoodMaster  $erpWoodMaster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ErpWoodMaster $erpWoodMaster)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ErpWoodMaster  $erpWoodMaster
     * @return \Illuminate\Http\Response
     */
    public function destroy(ErpWoodMaster $erpWoodMaster)
    {
        //
    }

    public function import(Request $request)
    {
        $data = json_decode($request->data,true);
            $model_master = new ErpWoodMaster;
             $model_master->name     =  $data['name'];
             $model_master->status     =   $data['status'];
             $model_master->client_id   =  $data['client_id'];        
             $model_master->created_by   = $data['admin_id'];
             $model_master->updated_by   = $data['admin_id'];
             $model_master->created_at   = date("Y-m-d H:i:s");
             $model_master->updated_at   = date("Y-m-d H:i:s");
             $model_master->is_deleted   = 0;
             $resultdata =  $model_master->save();
            if($resultdata){
                 $model_master_details = $data['importData']['body']; 
                 if(isset($model_master_details) && sizeof($model_master_details) > 0){
                    $row = 0;
                     foreach ($model_master_details as $master_detail) { 
                        $row++;
                        $validator = Validator::make($master_detail,[
                            'Barcode'=> 'required|string',
                            'AvgSED' => 'required|numeric',
                            'Volume' => 'required|numeric',
                            'CutLength' => 'required|numeric',
                            'Grade' => 'required|string'
                        ]);
                        if($validator->fails()){
                            return response()->json([
                                'status' => 'error',
                                'error-row' => $row
                            ]);
                        }
                        $circumference = $this->CircumferenceInFoot($master_detail['AvgSED']);
                        $master_detail['Cft'] =$circumference;
                        $model_details = new ErpWoodMasterDetails;
                        $model_details->master_id    = $model_master->_id;
                        $model_details->base         = $master_detail;
                        $model_details->created_by   = $data['admin_id'];
                        $model_details->updated_by   = $data['admin_id'];
                        $model_details->created_at   = date("Y-m-d H:i:s");
                        $model_details->updated_at   = date("Y-m-d H:i:s");
                        $model_details->is_deleted   = 0;       
                        if(!$model_details->save()){
                                error_log(print_r("Insert from vue".$master_detail, TRUE));
                        }
                    
                     }
                 }
                $model_master["admin_name"] = "Admin";
                return response()->json([
                 'status'   => 'success',
                 'msg' => $resultdata,
                 'master_list' => $model_master],200);
            }
            else{
                return response()->json([
                'status'   => 'error',
                'msg' => $resultdata],200);
            }
    }

    public function CircumferenceInFoot($d){
        return round($d*3.14*3.28084,2);
    }
}
