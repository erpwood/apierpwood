<?php
namespace App\Http\Middleware;
use Closure;
class Cors {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next) {
      $headers = [
        'Access-Control-Allow-Origin' => '*',
        'Access-Control-Allow-Methods'=> 'POST, GET, OPTIONS, PUT, DELETE',
        'Access-Control-Allow-Headers'=> 'Content-Type, X-Auth-Token, Origin, Authorization'
    ];

    if($request->getMethod() == "OPTIONS") {

        $response = new Response();
        foreach($headers as $key => $value)
            $response->headers->set($key, $value);

        return $response;
    }

    $response = $next($request);

    foreach($headers as $key => $value)
        $response->headers->set($key, $value);

    return $response;

        $response = $next($request);
      
		 $response->headers->set("Access-Control-Allow-Origin", "*");
    $response->headers->set("Access-Control-Allow-Credentials", "false");
    $response->headers->set("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    $response->headers->set("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, X-CSRF-TOKEN");
		 return $response;
		 
		 
    }
}