<?php


namespace App;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class ErpWoodMasterDetails extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'erpwood_master_details';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'barcode', 'lot','grade', 'length', 'sed', 'volume'
        //'name',
    ];
}